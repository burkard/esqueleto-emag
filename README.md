Esqueleto eMAG
=======================

Introdução
------------
Esta é um simples esqueleto para sites baseado no Modelo de Acessibilidade 
em Governo Eletrônico (eMAG versão 3.1) do governo federal brasileiro. Esta aplicação 
pode ser usada como ponto de partida para desenvolvimento de sites contendo 
recursos de acessibilidade e portabilidade.

[Documentação eMAG](http://emag.governoeletronico.gov.br/)

[Documentação ZF2 (em Inglês)](http://framework.zend.com/manual/2.0/en/index.html)