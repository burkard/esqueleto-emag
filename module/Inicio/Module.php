<?php
namespace Inicio;

class Module
{
    public function onBootstrap($e)
    {
        $translator = $e->getApplication()->getServiceManager()->get('translator');
        $translator
          ->setLocale('en_US')
          ->setFallbackLocale('pt_BR');

        $app = $e->getApplication();
        $sm = $app->getServiceManager();
        $translator = $sm->get('translator');

        // Attach the translator to the router
        $e->getRouter()->setTranslator($translator);    
    }
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
