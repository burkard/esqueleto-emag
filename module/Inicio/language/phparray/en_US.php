<?php

return array(
	'Título da Página' 	 => 'Website Title',
	'Ir para o conteúdo' => 'Go to content',
	'Ir para o menu' 	 => 'Go to menu',
	'Ir para a busca'    => 'Go to search',
	'Ir para o rodapé'   => 'Go to footer',
	'Alto Contraste'     => 'High Contrast',
	'Acessibilidade'     => 'Accessibility',
	'Mapa do Site'       => 'Site Map',
	'Buscar no site'     => 'Search this site',
	'Você está em'       => 'You are here',
	'Página Inicial'     => 'Home',
	'Topo da página'     => 'Back to top',
	'Todos os direitos reservados' => 'All rights reserved',
	'Universidade Federal de Santa Maria' => 'Federal University of Santa Maria',
);