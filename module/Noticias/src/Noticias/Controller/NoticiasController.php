<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Noticias\Controller;
 
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
 
class NoticiasController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }

    public function visualizarAction() {
    	$noticiaId = (int) $this->params()->fromRoute('id', 0);
    	return new ViewModel(array('noticiaId' => $noticiaId));
    }
}