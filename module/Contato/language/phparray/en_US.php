<?php

return array(
	'Contato' => 'Contact us',
	'Telefone' => 'Telephone',
	'Fone' => 'Phone',
	'Fale conosco' => 'Get in touch',
	'Onde estamos' => 'Where are we located',
);