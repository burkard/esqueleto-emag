<?php
 
return array(
    'controllers' => array(
        'invokables' => array(
            'HomeController' => 'Contato\Controller\HomeController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'contato' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options'   => array(
                    'route'    => '/contato',
                    'defaults' => array(
                        'controller' => 'HomeController',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'translator' => array(
        'translation_file_patterns' => array(
            array(
                'type'     => 'phparray',
                'base_dir' => __DIR__ . '/../language/phparray',
                'pattern'  => '%s.php',
            ),
        ),
    ),
    'view_manager' => array(
        'template_map'              => array(
            'contato/home/index'    => __DIR__ . '/../view/contato/index.phtml',
        ),
    ),
);