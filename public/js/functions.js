$(document).ready(function(){
 
	loadCSS = function(href, media, id) {
	    var cssLink = $("<link rel='stylesheet' type='text/css' href='"+href+"' id='" + id + "'  media='" + media + "'>");
	    $("head").append(cssLink); 
	};

	loadJS = function(src) {
	    var jsLink = $("<script type='text/javascript' src='"+src+"'>");
	    $("head").append(jsLink); 
	}; 

 	$('#siteaction-contraste > a').on('click', function(){
 		if ($('#css-alto-contraste').length) {
 			$('#css-alto-contraste').remove();
 		} else {
			loadCSS("/css/alto-contraste.css", "screen", "css-alto-contraste");
		}
	});
	

	$('#downloadQuickSearch').keyup(function(){
	   var valThis = $(this).val().toLowerCase();
	    $('.downloadList>li>h2').each(function(){
	     var text = $(this).text().toLowerCase();
	        (text.indexOf(valThis) >= 0) ? $(this).parent().show() : $(this).parent().hide();         
	   });
	});
	
});